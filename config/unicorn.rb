worker_processes 2
working_directory "/opt/association"

preload_app true

timeout 30

listen "/tmp/rails.sock"

pid "/opt/association/tmp/pids/unicorn.pid"

stderr_path "/opt/association/log/unicorn.stderr.log"
stdout_path "/opt/association/log/unicorn.stdout.log"

before_fork do |server, worker|
  defined?(ActiveRecord::Base) and 
    ActiveRecord::Base.connection.disconnect!
end

after_fork do |server, worker|
  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.establish_connection
end
