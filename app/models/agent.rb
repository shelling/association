class Agent < ActiveRecord::Base
  has_secure_password
  validates :password, length: { minimum: 6 }, allow_nil: true

  has_many :contacts, inverse_of: :agent
  accepts_nested_attributes_for :contacts, allow_destroy: true, reject_if: lambda {|attrs| attrs['phone'].blank? }
end
