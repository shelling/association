class Contact < ActiveRecord::Base
  belongs_to :agent, inverse_of: :contacts
  validates :agent, presence: true

  # another strategy is validates :default, uniqueness: true with the agent scope
  # which requires previous default contact to be unmark first
  before_save :unique_default_per_agent

  private

  def unique_default_per_agent
    self.agent.contacts.update_all(default: false)
  end
end
