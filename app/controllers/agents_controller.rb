class AgentsController < ApplicationController
  respond_to :html, :json

  before_action :set_agent, only: [:show, :edit, :update, :destroy]

  def index
    @agents = Agent.all
  end

  def show
  end

  def new
    @agent = Agent.new
  end

  def edit
  end

  def create
    @agent = Agent.new(agent_params)
    flash[:notice] = 'Agent was successfully created.' if @agent.save
    respond_with(@agent)
  end

  def update
    flash[:notice] = 'Agent was successfully updated.' if @agent.update(agent_params)
    respond_with(@agent)
  end

  def destroy
    @agent.destroy
    respond_with(@agent)
  end

  private

  def set_agent
    @agent = Agent.find(params[:id])
  end

  def agent_params
    params.require(:agent).permit!
  end
end
