class RemovePasswordFromAgent < ActiveRecord::Migration
  def change
    remove_column :agents, :password, :string, null: false, after: :name
  end
end
