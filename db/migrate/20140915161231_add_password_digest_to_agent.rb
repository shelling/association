class AddPasswordDigestToAgent < ActiveRecord::Migration
  def change
    add_column :agents, :password_digest, :string, null: false, after: :name
  end
end
