class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.integer   :agent_id,  null: false
      t.string    :phone,     null: false
      t.string    :label,     null: false
      t.boolean   :default,   null: false,  default: false
      t.timestamps
    end
  end
end
