class CreateAgents < ActiveRecord::Migration
  def change
    create_table :agents do |t|
      t.string    :name,      null: false
      t.string    :password,  null: false
      t.timestamps
    end
    add_index :agents, :name, unique: true
  end
end
